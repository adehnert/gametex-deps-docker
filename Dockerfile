FROM ubuntu:22.04

COPY preseed.txt preseed.txt
RUN apt-get update
RUN debconf-set-selections preseed.txt
ENV DEBIAN_FRONTEND=noninteractive 
RUN apt-get install -y texlive-latex-base texlive-latex-extra texlive-fonts-recommended texlive-pstricks
RUN apt-get install -y python3 make
# epub sheets
RUN apt-get install -y tex4ht calibre
