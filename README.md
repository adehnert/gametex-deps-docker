gametex-deps-docker
===================

This repo aims to build a Docker image with the dependencies for
[GameTeX](https://web.mit.edu/kenclary/Public/Guild/GameTeX/) and
[my addons](https://gitlab.com/adehnert/TestGame/-/tree/ebook/).

It's just an Ubuntu container with a few packages pre-installed; there's no
particularly compelling reason to use it, but it may make CI builds a bit
faster (giving a faster edit-compile-test cycle and potentially saving some CI
minutes) or could serve as documentation of the Ubuntu packages needed. Or
maybe you can actually use it locally? I've never tried that.
